#!/bin/ksh

BASEDIR="$HOME/code/cl/urt-web"
SBCL="/usr/local/bin/sbcl"
PIDF="/tmp/urt-web.pid"

menu() {
	echo "$0 <start|stop|kill>"
}

start() {
	$SBCL --script "${BASEDIR}/rc.lisp" &
	PID=$!
	echo $PID > $PIDF
	echo "======================="
	echo "set pidf:${PIDF} pid:${PID}" 
	echo "======================="
}

_stop() {
	PID=`cat $PIDF`
	echo "stop pid:[${PID}]"
	kill -9 $PID
	rm $PIDF 
}

# _status() ????

# blunt
_kill() {
	PID=`ps aux | grep sbcl | grep script | awk '{print $2;}'`
	echo "_kill pid:[${PID}]"
	kill -9 $PID
}

if [[ $# -lt 1 ]] ; then
	menu
	exit 1
fi

case "$1" in
"start") 
	echo "start" 
	start
	;;
"stop") 
	echo "stop" 
	_stop
	;;
"kill") 
	echo "kill" 
	_kill
	;;
*) 
	menu
	exit 1 
	;;
esac

