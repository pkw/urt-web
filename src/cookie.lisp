;; request.lisp has higher level cookie stuff.
(uiop:define-package #:urt-web/cookie
  (:use #:cl
        #:cl-base64)
  (:import-from #:ironclad)      
  (:export #:from-cookie
           #:to-cookie
           #:make-cipher
           #:render-cookie-date))

(in-package #:urt-web/cookie)

;; You must know ahead of time if you are dealing with encoded cookies.
(defun from-cookie (str)
  "Unescape cookie value."
  (base64-string-to-string str :uri t))

(defun to-cookie (str)
  "Escape cookie value."
  (string-to-base64-string str :uri t))

;; used ?
(defparameter +key+ "asd123j1kl2j3lk1j2kl3j1lkjl1l!@!#()!*@()#*")

;; used ?
(defun make-cipher ()
  (ironclad:make-cipher :blowfish
                        :mode :ecb
                        :key (ironclad:ascii-string-to-byte-array +key+)))

;; pass in (get-univeral-time)
;; (decode-universal-time (encode-universal-time 6 22 19 25 1 2002))
(defun render-cookie-date (time)
  "Returns a string representation of the universal time TIME
which can be used for cookie headers."
  (multiple-value-bind (second minute hour date month year weekday)
      (decode-universal-time time 0)
    (format nil "~A, ~2,'0d-~2,'0d-~4,'0d ~2,'0d:~2,'0d:~2,'0d GMT"
            (aref #("Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun") weekday)
            date month year hour minute second)))

#|
(defun encrypt (plaintext key)
  (let ((cipher (get-cipher key))
        (msg (ironclad:ascii-string-to-byte-array plaintext)))
    (ironclad:encrypt-in-place cipher msg)
    (ironclad:octets-to-integer msg)))

(defun decrypt (ciphertext-int key)
  (let ((cipher (get-cipher key))
        (msg (ironclad:integer-to-octets ciphertext-int)))
    (ironclad:decrypt-in-place cipher msg)
    (coerce (mapcar #'code-char (coerce msg 'list)) 'string)))

* (setf test (encrypt "A simple test message" "pass"))
* test
369463901797626567104213850270827601164336028018533
* (decrypt test "pass")
"A simple test message"
|#

