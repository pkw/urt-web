(uiop:define-package :urt-web/db
  (:import-from :dbi)
  (:export #:init
           #:get-db
           #:*db*))

(in-package :urt-web/db)

(defparameter %db-file% (asdf:system-relative-pathname "urt-web" "_data/db.sqlite3"))
(defparameter *db* nil)

(defun init () 
  (setf *db* (dbi:connect :sqlite3 :database-name %db-file%)))

(defun get-db ()
  (unless *db* (init))
  *db*)
