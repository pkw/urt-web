;; account/view is where sessions get created (and cookies).
(uiop:define-package :urt-web/session/session
  (:import-from :uuid)
  (:local-nicknames (#:db #:urt-web/session/db))
  (:export #:*session*
           #:uuid
           #:create))

(in-package :urt-web/session/session)

(defparameter *session* (make-hash-table)) 

(defun uuid () 
  "Abstract the specific uuid implementaion."
  (uuid:make-v1-uuid))

;; error handling ?
(defun create (db username)
  (let* ((uuid (format nil "~a" (uuid))))
    (format t "username:~a uuid:~a~%" username uuid)
    (db:create-session db username uuid)
  uuid))
