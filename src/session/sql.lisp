(uiop:define-package #:urt-web/session/sql
  (:export #:create-table-session
           #:insert-session
           #:username-from-session
           #:user-from-session
           #:delete-by-id))
(in-package #:urt-web/session/sql)

;; Convert all of this to sxql ? (but also why bother...)
;; Oh wait it's just comments, and 'suer-from-session is used.
;; Maybe I never really used regular old sessions.
;; Maybe I do this with cookies?
;; Anyways I'm looking to start using anonynour user sessions
;; in 'alive so i'll poke around.
;;  i think i will not use sessions or cookies.
;; Cookies are in request and would probably work right now.
;; Session stuff allow could be added for when you want less
;; cookies (still relies on the one session one though).

(defun create-table-session ()
"
-- id is a uuid, so it can't be predicted. it is the session cookie value.
-- username is a pseudo foreign key to account.user.username
CREATE TABLE IF NOT EXISTS session (
  id TEXT NOT NULL UNIQUE,
  username TEXT NOT NULL, -- can have multiple sessions per user, also psuedo FK
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
")

(defun insert-session ()
"
INSERT INTO session
(id, username)
VALUES
( ? , ? )
")

(defun username-from-session ()
"
SELECT username
FROM session
WHERE id = ?
")

(defun user-from-session ()
  "this joins user.username/session.username"
"
SELECT username 
FROM user 
WHERE username = (
  SELECT username 
  FROM session 
  WHERE
  id = ?
)
")

(defun delete-by-id ()
"
DELETE FROM session
WHERE id = ?
")
