(uiop:define-package :urt-web/session/db
  (:import-from #:dbi)
  (:local-nicknames (#:sql #:urt-web/session/sql))
  (:export #:create-session-table
           #:create-session
           #:get-username-from-session
           #:get-user-from-session
           #:delete-by-uuid))

(in-package :urt-web/session/db)

(defun create-session-table (db)
  (dbi:do-sql db (sql:create-table-session)))

(defun create-session (db username id)
  (dbi:do-sql db 
    (sql:insert-session)
    (list id username)))

;; maybe this it's sql are depr.
(defun get-username-from-session (db uuid)
  (format t "get-username... uuid:~a~%" uuid) 
  (car
    (dbi:fetch-all 
      (dbi:execute 
        (dbi:prepare db (sql:username-from-session))
        (list uuid)))))

(defun get-user-from-session (db uuid)
  (format t "get-user... uuid:~a~%" uuid) 
  (let ((user
   (car
    (dbi:fetch-all 
           (dbi:execute 
            (dbi:prepare db (sql:user-from-session))
            (list uuid))))))
    (format t "--user:~s~%" user)
    (getf user :|username|)))

(defun delete-by-uuid (db uuid)
  (dbi:do-sql db 
    (sql:delete-by-id)
    (list uuid)))
