(uiop:define-package #:urt-web/route
  (:export #:route))

(in-package #:urt-web/route)

;; this would get passed to the serve function, and then
;; that would take care of generalizing it.
;; OR you could just pass the *routes* hash-table to the
;; serve function under the same auspices.
(defclass router () ())

(defun route (path-info &key routes default)
  (let ((view (gethash path-info routes)))
    (when (null view)
      (setf view default))
    view))

#|
Make a 'url' class that will generate a table of url's based on
some basic module level hash-table / object.

search-urls: (('/index' . #index) ('/search/action-1' . #'action-1))

or more complicated:
search-urls: 
(('/index' . (list #'index ())) 
 ('/search/action-1' . (list #'action-1 (list #'ensure-user))))

This could either just be a url generation convienience or also
lead to reverse urls.

ALSO could add the security function to call like #'endure-user.
Maybe it's a list and the default nil is just normal.
|#
