(uiop:define-package #:urt-web/serve
  (:use #:cl
        #:urt-web/web
        #:urt-web/request)
  (:import-from #:cffi)
  (:local-nicknames (#:header #:urt-web/header)
                    (#:config #:urt-web/config)
                    (#:route #:urt-web/route))
  (:export #:serve))
(in-package #:urt-web/serve)

(defun serve (&key server-config
                   routes
                   default-route
                   db)
  (format t "---SERVE---~%")
  ;;(format t "server-config:~s~%" server-config)
  (fcgx-init)
  (let* ((req (cffi:foreign-alloc :char :count (myfcgi-request-type-size)))
         (port-str (format nil ":~a" (config:port server-config)))
         (sockfd (fcgx-opensocket port-str 1024)))
    (fcgx-initrequest req sockfd 0)

    (loop while (= 0 (fcgx-accept-r req))
          do ;; view-fn must be called ahead of use to allow it to set headers
          (let* ((request (make-request :c-req req :db db))
                 (view-fn (route:route (path-info request) :routes routes :default default-route))
                 (view-fn-output (funcall view-fn request)))

            ;; Print/Send Headers (including cookies)
            (dolist (i (headers request))
              (format t "serve: header n:~a v:~a~%" (car i) (cdr i))
              (let* ((name (car i))
                     (val (cdr i))
                     (hdr (format nil "~a: ~a~a~a" name val #\return #\linefeed)))
                (fcgx-fprintf (myfcgi-request-out req) hdr)))

            (fcgx-fprintf (myfcgi-request-out req) (header:content-type (content-type request)))

            (fcgx-fprintf (myfcgi-request-out req) view-fn-output))

          (fcgx-finish req))

    (fcgx-free req sockfd)
    (cffi:foreign-free req)))
