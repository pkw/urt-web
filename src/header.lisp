(uiop:define-package :urt-web/header
  (:export #:html-header
           #:content-type))
(in-package :urt-web/header)

;; is content-type special or just typically the last header followed by and extr \r\n ?
(defun content-type (type)
  (format nil "Content-type: ~a~a~a~a~a" type #\return #\linefeed #\return #\linefeed))

(defun html-header ()
  ;;(format nil "Content-type: text/html~a~a~a~a" #\return #\linefeed #\return #\linefeed)
  (content-type "text/html"))
