(uiop:define-package #:urt-web/config
  (:use #:cl
        #:file-config)
  (:export #:make-server-config
           #:server-config
           #:port
           #:smtp-host
           #:smtp-user
           #:smtp-password
           #:base-url))
(in-package #:urt-web/config)

;; This is useful even if it has no slots defined at
;; the base level.  Don't be afraid to remove/move
;; them.
;;
;; Possible addtions: site-name ... like [alive] (not the square brackets
;;                    email-from  ... alive@qwd.qwd
(defclass server-config (file-config)
  ((port :accessor port
         :initform nil)
   (smtp-host :accessor smtp-host
              :initform nil)
   (smtp-user :accessor smtp-user
              :initform nil)
   (smtp-password :accessor smtp-password
                  :initform nil)
   (base-url :accessor base-url
             :initform nil)
   (tmp-dir :accessor tmp-dir
            :initform nil)))

(defun make-server-config (file-name)
  (let ((c (make-instance 'server-config)))
    (from-file c file-name)
    c))
