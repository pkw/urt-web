(uiop:define-package #:urt-web/main
  (:import-from #:trivia)
  (:import-from #:urt-web/serve #:serve)
  (:local-nicknames (#:config #:urt-web/config)
                    (#:view #:urt-web/view)
                    (#:db #:urt-web/db))
  (:export #:main
           #:run))

(in-package #:urt-web/main)

(defun menu () 
  (format t "[Urt-Web]~%")
  (format t "  run: run server, this should be a good generalized example.~%"))

(defun main (&optional args) 
  (unless args (setf args (uiop:command-line-arguments)))
  (trivia:match args 
                ((list "run")
                 (run))
                ((list* _)
                 (menu))))

(defun get-routes ()
  (let ((routes (make-hash-table :test 'equalp)))
    (setf (gethash "" routes) #'view:main)
    (setf (gethash "/home/" routes) #'view:home)
    routes))

(defun run ()
  (let* ((cfg-f (asdf:system-relative-pathname "urt-web" "_data/config.lisp"))
         (cfg (config:make-server-config cfg-f)))
    (format t "urt-web run ...~%")
    (serve :server-config cfg
           :default-route #'view:default-view
           :routes (get-routes)
           :db (db:get-db))))
