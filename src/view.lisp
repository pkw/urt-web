(uiop:define-package #:urt-web/view
  (:mix #:cl 
        #:spinneret)
  (:export #:main
           #:home
           #:default-view
           #:redirect))

(in-package #:urt-web/view)

(defun main (req)
  (declare (ignore req))
  (with-html-string
    (:doctype)
    (:html
     (:head 
      (:title "main"))
     (:body
      (:h1 "MAIN")
      (:a :href "/home/" "home")))))

(defun home (req)
  (declare (ignore req))
  (with-html-string
    (:doctype)
    (:html
     (:head 
      (:title "home"))
     (:body
      (:h1 "HOME")
      (:a :href "/" "main")))))

(defun default-view (req) 
  (declare (ignore req))
  (with-html-string
    (:doctype)
    (:html
     (:head 
      (:title "defview"))
     (:body
      (:h1 "default view")))))

(defun redirect (url &key (timeout 0))
  (with-html-string
    (:doctype)
    (:html
      (:head 
        (:title "redirect")
        (:meta :http-equiv "Refresh" :content (format nil "~a; URL=~a" timeout url))
        (:meta :http-equiv "Content-Type" :content "text/html; charset=utf-8")))))

