(uiop:define-package :urt-web/serve
  (:use #:cl
        #:urt-web/web
        #:urt-web/request)
  (:import-from #:cffi)
  (:import-from #:urt-web/db #:get-db)
  (:export #:serve))

(in-package :urt-web/serve)

(defun serve ()
  (urt-web/db:init)
  (fcgx-init)
  ;; 'req' is low-level fastcgi/cffi thing.
  ;; 'request' is high-level web/clos thing.
  (let ((req (cffi:foreign-alloc :char :count (myfcgi-request-type-size)))
        (sockfd (fcgx-opensocket ":8010" 1024)))
    (fcgx-initrequest req sockfd 0)

    (loop while (= 0 (fcgx-accept-r req))
          do ;; view-fn must be called ahead of use to allow it to set headers
          (let* ((request (make-request :c-req req :db (get-db)))
                 (view-fn (urt-web/route:route (path-info request)))
                 (view-fn-output (funcall view-fn request)))

            ;; Print/Send Headers (including cookies)
            (dolist (i (headers request))
              (format t "header n:~a v:~a~%" (car i) (cdr i))
              (let* ((name (car i))
                     (val (cdr i))
                     (hdr (format nil "~a: ~a~a~a" name val #\return #\linefeed)))
                (fcgx-fprintf (myfcgi-request-out req) hdr)))

            (myfcgi-text-html-header req)

            (fcgx-fprintf (myfcgi-request-out req) view-fn-output))

          (fcgx-finish req))

    (fcgx-free req sockfd)
    (cffi:foreign-free req)))
