;; this all should be moved to model.lisp
(uiop:define-package :urt-web/account/db
  (:import-from :dbi)
  (:local-nicknames (#:sql #:urt-web/account/sql))
  (:export #:create-user-table
           #:get-user))

(in-package :urt-web/account/db)

;; This needs to exist, the code automatically
;; looks for a user here.
(defun create-user-table (db)
  (dbi:do-sql db (sql:create-table-user)))

(defun get-user (db username)
  (format t "get-user username:~a~%" username)
  (car
    (dbi:fetch-all 
      (dbi:execute 
        (dbi:prepare db (sql:select-user))
        (list username)))))

