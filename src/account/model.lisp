;; TODO: The account/db.lisp stuff should be moved here.
(uiop:define-package #:urt-web/account/model
  (:use #:cl 
        #:sxql)
  (:import-from #:dbi)
  (:import-from #:str)
  (:import-from #:random-state)
  (:local-nicknames (#:i #:iterate))
  (:export #:insert-user
           #:create-register-table
           #:create-register-key
           #:insert-register
           #:get-register
           #:get-reset 
           #:get-user
           #:enable-user
           #:delete-register
           #:delete-reset
           #:create-reset-table
           #:insert-reset
           #:set-password))
(in-package #:urt-web/account/model)

(defparameter *key-chars* "0123456789abcdefghijklmnopqrstuvwxyz")

(defun insert-user (db username email)
  "Insert user record, and return nil as failure."
  (let ((sql (insert-into :user 
               (set= :username username
                     :email email
                     :password (default-password)))))
    (multiple-value-bind (s v) (yield sql)
      (handler-case (dbi:do-sql db s v)
        (dbi.error:dbi-database-error (e)
          (format t "insert-user error e:~a~%" e)
          (return-from insert-user nil))))))

(defun get-user (db username email)
  (let ((sql (select (:*)
               (from :user)
               (where (:and (:= :username username)
                            (:= :email email)))
               (limit 1))))
    (multiple-value-bind (s v) (yield sql)
      (let* ((query0 (dbi:prepare db s))
             (query (dbi:execute query0 v))
             (row (dbi:fetch query)))
        row))))

(defun create-register-table (db)
  "Create register table."
  ;; Nuance:
  ;; You can potentially have one email register multiple
  ;; usernames, and ostensibly this allows for that.
  (let ((sql "
CREATE TABLE IF NOT EXISTS register (
  username TEXT NOT NULL UNIQUE,
  email TEXT NOT NULL,
  key TEXT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (username) REFERENCES user(username),
  PRIMARY KEY (username, email)
)
        "))
    (dbi:do-sql db sql)))

(defun create-reset-table (db)
  (let ((sql "
CREATE TABLE IF NOT EXISTS reset (
  username TEXT NOT NULL UNIQUE,
  email TEXT NOT NULL,
  key TEXT NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (username) REFERENCES user(username),
  PRIMARY KEY (username, email)
)
        "))
    (dbi:do-sql db sql)))

(defun create-register-key ()
  (let* ((len (length *key-chars*))
         (key-len 24)
         (key (i:iter (i:for i from 0 to key-len)
                      (i:collect 
                        (str:s-nth 
                          (random-state:random-int 'mersenne-twister-64 0 (- len 1)) 
                          *key-chars*)))))
    (format nil "~{~a~}" key)))

(defun default-password ()
  (let* ((len (length *key-chars*))
         (key-len 8)
         (key (i:iter (i:for i from 0 to key-len)
                      (i:collect 
                        (str:s-nth 
                          (random-state:random-int 'mersenne-twister-64 0 (- len 1)) 
                          *key-chars*)))))
    (format nil "~{~a~}" key)))

(defun insert-register (db username email)
  (let ((sql (insert-into :register 
               (set= :username username
                     :email email
                     :key (create-register-key)))))
    (multiple-value-bind (s v) (yield sql)
      (handler-case (dbi:do-sql db s v)
        (dbi.error:dbi-database-error (e)
          (format t "insert-register error e:~a~%" e)
          (return-from insert-register nil))))))

(defun get-register (db username) ;; TODO also match email
  (let ((sql (select (:key)
               (from :register)
               (where (:= :username username))
               (limit 1))))
    (multiple-value-bind (s v) (yield sql)
      (let* ((query0 (dbi:prepare db s))
             (query (dbi:execute query0 v))
             (row (dbi:fetch query)))
        (getf row :|key|)))))

(defun enable-user (db username)      
  (let ((sql (update :user 
               (set= :enabled 1) ;; 1 for true ;; using CL t is error
               (where (:= :username username)))))
    (multiple-value-bind (s v) (yield sql)
      (format t "s:~s v:~s~%" s v)
      (dbi:do-sql db s v))))

(defun delete-register (db username)
  (let ((sql (delete-from :register
               (where (:= :username username)))))
    (multiple-value-bind (s v) (yield sql)
      (dbi:do-sql db s v))))

(defun delete-reset (db username)
  (let ((sql (delete-from :reset
               (where (:= :username username)))))
    (multiple-value-bind (s v) (yield sql)
      (dbi:do-sql db s v))))

(defun insert-reset (db username email)
  (let ((sql (insert-into :reset 
               (set= :username username
                     :email email
                     :key (create-register-key))))) ;; re-use create-register-key
    (multiple-value-bind (s v) (yield sql)
      (handler-case (dbi:do-sql db s v)
        (dbi.error:dbi-database-error (e)
          (format t "insert-reset error e:~a~%" e)
          (return-from insert-reset nil))))))

(defun get-reset (db username email)
  (let ((sql (select (:key)
               (from :reset)
               (where (:and (:= :username username)
                            (:= :email email)))
               (limit 1))))
    (multiple-value-bind (s v) (yield sql)
      (let* ((query0 (dbi:prepare db s))
             (query (dbi:execute query0 v))
             (row (dbi:fetch query)))
        (getf row :|key|)))))

(defun set-password (db username password)
  (let ((sql (update :user 
               (set= :password password) 
               (where (:= :username username)))))
    (multiple-value-bind (s v) (yield sql)
      (dbi:do-sql db s v))))

