(uiop:define-package :urt-web/account/sql
  (:export #:create-table-user
           #:select-user))

(in-package :urt-web/account/sql)

(defun create-table-user ()
"
CREATE TABLE IF NOT EXISTS user (
  username TEXT PRIMARY KEY,
  email TEXT NULL,
  password TEXT NULL,
  enabled BOOLEAN DEFAULT FALSE,
  admin BOOLEAN DEFAULT FALSE,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated TIMESTAMP NULL
)
")

(defun select-user ()
"
SELECT username, password
FROM user
WHERE username = ?
")
