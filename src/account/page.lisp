;; this could be moved to urt-web ?
;; then just view needs to be overridder?
;; (then maybe that can be organized, like serve.lisp was?)
(uiop:define-package #:urt-web/account/page
  (:use :cl 
        :spinneret)
  (:import-from #:urt-web/account/forms #:make-login-form #:username #:password)
  (:export #:login-page
           #:logout-page))

(in-package #:urt-web/account/page)

(defun login-page (form) 
  (with-html 
    (:br)
    (:form :method "post"
      (:label "username")
      (:input :type "text"
              :name "username"
              :id "username"
              :autocomplete "username"
              :value (username form))
      (:br)
      (:label "password")
      (:input :type "password"
              :name "password"
              :id "password"
              :autocomplete "current-password"
              :value (password form))
      (:br)
      (:button "Login")
      (:a :href "/account/login/" "Clear Post")
      (:a :href "/account/reset/" "Reset Password"))))

(defun logout-page ()
  (with-html 
    (:p "Do you want to logout?")
    (:a :href "/account/logout/?f=1" :class "w3-button w3-lime" "Yes Logout")))



