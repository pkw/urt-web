(uiop:define-package :urt-web/account/register
  (:local-nicknames (#:smtp #:cl-smtp)
                    (#:model #:urt-web/account/model)
                    (#:config #:urt-web/config))
  (:export #:send
           #:register-url
           #:register-body))

(in-package :urt-web/account/register)

;;"https://clue.d34d.net/account/register-confirm/?k=qwdqwdqw12312dqwdqw")
(defun register-url (db username &key base-url)
  (let ((key (model:get-register db username)))
    (format nil 
            "http://~a/account/register-confirm/?k=~a"
            base-url
            key)))

(defun register-body (db username &key base-url)
  (let ((url (register-url db username :base-url base-url)))
    (format nil "Visit this link to complete registration: ~a" url)))

;; add site arg and from keywords with defaults
;; or jam it all into config ...
(defun send (db username email &key config)
  (let ((user (config:smtp-user config))
        (host (config:smtp-host config))
        (password (config:smtp-password config)))
    (format t "register:send email:~a~%" email)
    (format t "register:send user:~s host:~s password:~s~%" user host password)
    (smtp:send-email host 
                     "task@oldcode.org"
                     email 
                     "Register"
                     (register-body db username :base-url (config:base-url config))
                     :port 587
                     :authentication (list user password)
                     :ssl :starttls)))

