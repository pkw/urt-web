(uiop:define-package :urt-web/account/account
  (:import-from #:urt-web/account/db #:get-user)
  (:local-nicknames (#:session #:urt-web/session/session))
  (:export #:login
           #:login-matches-db))

(in-package :urt-web/account/account)

(defun login-matches-db (db username password)
  (when (or (or (null username) (equal username ""))
            (or (null password) (equal password "")))
    (return-from login-matches-db nil))

  (let* ((user (get-user db username))
         (db-password (getf user :|password|)))
    (format t "get-user: user:~a db-pwd:~a~%" user db-password)
    (if (equal password db-password)
      t
      nil)))

(defun login (db username password)
  "Check that USERNAME and PASSWORD are correct for the user.
Create a session entry. 
HHHStore session entry id in a session cookie. do this back in view ...?
*Delete any previous session cookie.
*If there is a previous cookie delete it's session entry?"
  (format t "account:login...~%")
  (let ((ok (login-matches-db db username password)))
    (format t "login ok:~a~%" ok)
    (when ok
      (let ((uuid (session:create db username)))
        uuid))))

