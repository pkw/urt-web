;; this is just ensure-user right now
(uiop:define-package #:urt-web/account/view
  (:use #:cl 
        #:spinneret)
  (:local-nicknames (#:request #:urt-web/request))
  (:export #:ensure-user))

(in-package #:urt-web/account/view)

(defun block-page ()
  (with-html-string
    (:doctype)
    (:html
     (:head (:title "computer says"))
     (:body (:p "no")))))

(defun ensure-user (view-fn)
  "Wrap a view function and check if there is a logged in user.
If not don't proceed.
A view-fn takes a request arg."
  (lambda (r)
    (let ((user (request:user r)))
      (if user
        (progn
          (format t "ensure-user -- YES USER~%")
          (funcall view-fn r))
        (progn
          (format t "ensure-user --- NO USER~%")
          (block-page))))))

