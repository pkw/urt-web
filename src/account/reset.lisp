(uiop:define-package :urt-web/account/reset
  (:local-nicknames (#:smtp #:cl-smtp)
                    (#:model #:urt-web/account/model)
                    (#:config #:urt-web/config))
  (:export #:send
           #:reset-url
           #:reset-body))

(in-package :urt-web/account/reset)

(defun reset-url (db username email &key base-url)
  (let ((key (model:get-reset db username email)))
    (format nil 
            "http://~a/account/reset-confirm/?k=~a"
            base-url
            key)))

(defun reset-body (db username email &key base-url)
  (let ((url (reset-url db username email :base-url base-url)))
    (format nil "Visit this link to complete password reset: ~a" url)))

;; do the same config as register (keywords or config) 
(defun send (db username email &key config)
  (let ((user (config:smtp-user config))
        (host (config:smtp-host config))
        (password (config:smtp-password config)))
    (format t "reset:send email:~a~%" email)
    (smtp:send-email host 
                     "task@oldcode.org"
                     email 
                     "Reset"
                     (reset-body db username email :base-url (config:base-url config))
                     :port 587
                     :authentication (list user password)
                     :ssl :starttls)))

