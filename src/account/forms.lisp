(uiop:define-package :urt-web/account/forms
  (:import-from #:urt-web/form #:form)
  (:export #:login-form
           #:make-login-form
           #:username
           #:password))

(in-package :urt-web/account/forms)

(defclass login-form (form)
  ((username :accessor username
             :initform nil)
   (password :accessor password
             :initform nil)))

(defun make-login-form () (make-instance 'login-form))

