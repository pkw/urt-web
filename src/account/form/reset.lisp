;; maybe this can be moved to urt-web ?
(uiop:define-package :urt-web/account/form/reset
  (:import-from #:urt-web/form #:form)
  (:export #:reset-form
           #:make-reset-form
           #:username
           #:email
           #:password
           #:password2))

(in-package :urt-web/account/form/reset)

(defclass reset-form (form)
  ((username :accessor username :initform nil)
   (email :accessor email :initform nil)
   (password :accessor password :initform nil)
   (password2 :accessor password2 :initform nil)))

(defun make-reset-form () (make-instance 'reset-form))

