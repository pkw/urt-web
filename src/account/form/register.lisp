;; maybe this can be moved to urt-web ?
(uiop:define-package :urt-web/account/form/register
  (:import-from #:urt-web/form #:form)
  (:export #:register-form
           #:make-register-form
           #:register-confirm-form
           #:make-register-confirm-form
           #:username
           #:email))

(in-package :urt-web/account/form/register)

(defclass register-form (form)
  ((username :accessor username :initform nil)
   (email :accessor email :initform nil)))

(defun make-register-form () (make-instance 'register-form))

(defclass register-confirm-form (form)
  ((username :accessor username :initform nil)))

(defun make-register-confirm-form () (make-instance 'register-confirm-form))
