;; CLOS request is the high level request (also response, context).
;;   It (along with web.lisp) wraps a lot of the cffi/fastcgi stuff.
(uiop:define-package #:urt-web/request
  (:use #:cl
        #:alexandria
        #:urt-web/web)
  (:import-from #:cffi)
  (:import-from #:urt-web/util)
  (:import-from #:urt-web/session/session)
  (:export #:request
           #:make-request
           #:c-req
           #:headers
           #:user
           #:content-type
           #:path-info
           #:query-string
           #:content-length
           #:request-method
           #:add-response-header
           #:postp
           #:cookies
           #:get-post-form-vars
           #:add-cookie
           #:get-cookie
           #:del-cookie
           #:session-id))
(in-package #:urt-web/request)

;; maybe request-context would be a better name. (or request-response-context)
;; then it (add-response-header <request-context-instance>) would read better.
;; OR ... just context ? in the web-server framework this is the most fundamental
;; 'context' so it's ok if it takes that general name.
(defclass request ()
  ((c-req :accessor c-req
          :initarg :c-req
          :initform nil)
   (user :accessor user ;; this is just the string username
         :initform nil)
   (headers :accessor headers ;; RESPONSE headers
            :initform '())
   (response-content-type :accessor content-type
                          :initform "text/html")))

;; save/cache cookies in request ?
(defun make-request (&key c-req db)
  (format t "in request:make-request ... check for session cookie and set user ...~%")
  (let* ((request (make-instance 'request :c-req c-req))
         (cookies (cookies request))
         (session-cookie (cdr (assoc "session" cookies :test 'equal))))
    (format t "session-cookie:~a~%" session-cookie)
    (when session-cookie
      (let ((user (urt-web/session/db:get-user-from-session
                   db
                   session-cookie)))
        (format t "set request user:~s~%" user)
        (setf (user request) user)))
    request))

(defmethod session-id ((r request))
  (let* ((cookies (cookies r))
         (session-cookie (cdr (assoc "session" cookies :test 'equal))))
    (format t "--session-cookie:~a~%" session-cookie)
    session-cookie))

(defmethod path-info ((r request))
  (myfcgi-get-param (c-req r) "PATH_INFO"))

(defmethod query-string ((r request))
  (myfcgi-get-param (c-req r) "QUERY_STRING"))

(defmethod request-method ((r request))
  (myfcgi-get-param (c-req r) "REQUEST_METHOD"))

;; probably need to handle SB-INT:SIMPLE-PARSE-ERROR
;; (or just ERROR)
(defmethod content-length ((r request))
  (parse-integer (myfcgi-get-param (c-req r) "CONTENT_LENGTH")))

(defmethod add-response-header ((r request) name val)
  (format t "request: add-response-header: name:~a val:~a~%" name val) 
  (let ((i))
    ;;(setf (gethash name (headers r)) val)
    (push name i)
    (setf (cdr i) val)
    (push i (headers r))))

(defmethod postp ((r request))
  (equalp "POST" (request-method r)))

;; This might be a "consume the stream" type of thing.
;; Meaning you can't run this a second time.
;;
;; Investigate not allocating cffi memory.
;; Maybe use pre-allocated memory?
(defmethod get-post-form-vars ((r request))
  (when (postp r)
    (let* ((content-length (content-length r))
           (buf (cffi:foreign-alloc :char :count content-length))
           (in (urt-web/web:myfcgi-request-in (c-req r))))
      (urt-web/web:fcgx-get-str buf content-length in)
      (let* ((buf-str (cffi:foreign-string-to-lisp buf :count content-length))
             (vars (urt-web/util:parse-form-vars buf-str)))
        (format t "request:get-post-form-vars:buf-str:~s~%" buf-str)
        (cffi:foreign-free buf)
        vars))))
        ;; remove K:nil v:"" ;; maybe that should happen in parse-form-vars
        ;;(maphash (lambda (k v) (unless k (remhash k vars))) vars)))))
 

(defmethod cookies ((r request))
  (let ((raw-cookies (myfcgi-get-param (c-req r) "HTTP_COOKIE")))
    (urt-web/util:parse-http-cookie raw-cookies)))

(defmethod add-cookie ((r request) name value)
  "This sets string value as cookie. name and value need to be escaped.
Ie. they can't have spaces etc."
  (add-response-header
   r
   "Set-Cookie"
   (format nil "~a=~a; Path=/; SameSite=Strict; HttpOnly"
	   name
	   value)))

(defmethod get-cookie ((r request) name)
  (cdr (assoc name (cookies r) :test 'equal)))

;;expires=Thu, 01 Jan 1970 00:00:00 GMT; ;; UTC vs. GMT ?
(defmethod del-cookie ((r request) name)
  (add-response-header
   r
   "Set-Cookie"
   (format
    nil
    "~a=delete; Path=/; SameSite=Strict; HttpOnly; expires=Thu, Jan 01 1970 00:00:00 UTC;"
    name)))
