(format t "run urt-web...~%")

(require :asdf) 

;; require uiop, and do uiop:getenv stuff for HOME
(let ((quicklisp-init #P"/home/wise/quicklisp/setup.lisp"))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload 'urt-web)
;;(asdf:load-system 'urt-web)

(urt-web:serve)




;;sbcl --load myprog.asd --eval '(require :mypackage)' --eval '(mypackage:main)'
;; https://lispcookbook.github.io/cl-cookbook/os.html
