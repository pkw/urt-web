(uiop:define-package :urt-web/dev1
  (:mix :cl)
  (:export #:server-thread))

(in-package :urt-web/dev1)

#|
SB-INT:SIMPLE-PARSE-ERROR 
TYPE-ERROR

  (handler-case
    (dbi:do-sql db
"
INSERT INTO servers
(ip, port)
VALUES 
(?, ?)
" 
      (list ip port))
    (dbi.error:dbi-database-error (e) 
      (let ((code (dbi:database-error-code e))
            (message (dbi:database-error-message e)))
|#

(defun server-thread ()
  "Start a server by calling sbcl in a thread
so it might be killable."
  (format t "server-thread...~%")
  (let ((thr (bt:make-thread (lambda ()
      (uiop:run-program 
        (list "sbcl" "--script"  "/home/wise/code/cl/urt-web/rc.lisp")
        :name "<<<server>>>"
        :force-shell nil  
        :output t)))))
    thr))


#|
(defun insert-error-test ()
  (format t "insert-error-test...~%")
  (urt-web/db:init-db)
  (let ((db (urt-web/db:get-db)))
    (urt-web/urt/db:insert-or-update-server db "123.123.123.999" 99992)))

|#
