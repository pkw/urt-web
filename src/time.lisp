(uiop:define-package :urt-web/time
  (:mix :cl :local-time)
  (:export #:time1))

(in-package :urt-web/time)

(defun time1 ()
  (format t "time1...~%")
  (now)
  (format t "~a~%" +asctime-format+))

#|
CL-USER> (lt:format-timestring nil (lt:now)
                               :format lt:+asctime-format+)
"Wed Dec 16 13:45:24 2020"

CL-USER> (lt:reread-timezone-repository)
NIL

CL-USER> lt::*default-timezone-repository-path*
#P"/home/user/quicklisp/local-projects/local-time/zoneinfo/"

CL-USER> (lt:find-timezone-by-location-name "US/Eastern")
#<LT::TIMEZONE LMT EDT EST EWT EPT>
T

CL-USER> (lt:format-timestring nil (lt:now)
                               :format lt:+asctime-format+
                               :timezone *)
"Wed Dec 16 09:00:57 2020"
|#
