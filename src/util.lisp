(uiop:define-package :urt-web/util
  (:use #:cl #:trivia)
  (:import-from #:alexandria #:make-keyword)
  (:import-from #:quri #:url-decode)
  (:import-from #:str #:split)
  (:export #:parse-form-vars
           #:parse-form-vars-2
           #:parse-http-cookie
           #:vars-alist))

(in-package :urt-web/util)

(defun parse-form-vars (vars-str)
  "Parse a query string into a hash-table of its key, values.
Keys with multiple values get assigned a list of those values.
Return hash-table with caseinsens. string keys."
  (let ((l (split "&" vars-str))
        (ht (make-hash-table :test 'equalp)))
    (dolist (i l)
      (let* ((ss (split "=" i :omit-nulls t))
             (key (nth 0 ss))
             (new-val (ignore-errors (url-decode (nth 1 ss))))) ;; TODO:move this into if and dont need to ignore-errors
        (if (= 2 (length ss))
          (let ((val (gethash key ht)))
            (match val
              ((null) ;; no value set yet, set it
               ;; same as (satisfies null) ;;'null' is a pred-fn or a macro keyword
               (setf (gethash key ht) new-val))
              ((satisfies stringp) ;; just a previous string, so replace that with a list
               (setf (gethash key ht) (list val new-val)))
              ((cons _ _) ;; we already have a list started, add to it
               (setf (gethash key ht) (cons new-val (gethash key ht))))))

          ;; else 
          (when ss ;; don't add a entry like: K:NIL V:""
            (setf (gethash key ht) "")))))
    ht))

;; Don't use this if you need multi-values per name.
(defun parse-form-vars-2 (vars-str)
  "return hash-table with keyword keys (case insensitive?)"
  (let ((l (split "&" vars-str))
        (ht (make-hash-table :test 'equalp)))
    (dolist (i l)
      (let* ((ss (split "=" i :omit-nulls t))
             (key (make-keyword (string-upcase (nth 0 ss)))))
        (if (= 2 (length ss))
          (setf (gethash key ht) (url-decode (nth 1 ss)))
          (setf (gethash key ht) nil))))
    ht))

;; probably catch some errors
;;dousee=123123kj1l23jkl12j3; ckckc=999; urtweb9029=zHEY123123a3fWa
(defun parse-http-cookie (cookie-header)
  (format t "util:parse-http-cookie: cookie-header:~s~%" cookie-header)
  (let* ((l (split " " cookie-header))
         (cookies))
    (dolist (c l)
      (let* ((s (string-trim ";" c))
             (n-v (split "=" s))
             (n (car n-v))
             (v (nth 1 n-v)))
        (push (cons n v) cookies)))
    cookies))

(defun vars-alist (vars-str)
  "This makes a query-param type of url string into an alist."
  (let ((l (split "&" vars-str))
        (r))
    (dolist (i l)
      (let ((ss (split "=" i :omit-nulls t)))
        (if (= 2 (length ss))
          (push (cons (car ss) (url-decode (cadr ss))) r)
          (push (cons (car ss) nil) r))))
    r))
  



