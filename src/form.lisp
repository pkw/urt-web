(uiop:define-package #:urt-web/form
  (:mix :closer-mop
        :cl)
  (:export #:form
           #:init
           #:*nil*
           #:init-from-db))
(in-package #:urt-web/form)

(defparameter *nil* "-nil-")

(defclass form () ())

;; This should be called init-from-post
;; (form:init form post-vars)
(defmethod init ((f form) data)
  "Set incoming POST form vars to matching slots."
  (dolist (slot (class-slots (class-of f)))
    (let* ((slot-name (slot-definition-name slot))
           (slot-str-name (symbol-name slot-name)))

      ;; shouldn't i look up the hash value directly instead of looping each time ?
      (maphash 
        #'(lambda (k v)
          ;;(format t "form.init maphash k:~s v:~s~%" k v)
          (when (equalp k slot-str-name)
            ;;(format t "form.init set slot: slot-name:~a v:~a~%" slot-name v)
            (setf (slot-value f slot-name) v)))
        data))))

(defmethod init-from-db ((f form) data)
  "Set form vars from db. :|title| ..."
  ;;(format t "init-from-db: data:~s~%" data)
  (dolist (slot (class-slots (class-of f)))
    (let* ((slot-name (slot-definition-name slot))
           (slot-str-name (symbol-name slot-name))
           (slot-kw (intern (string-downcase slot-str-name) :keyword)))
      ;;(format t "slot-name:~s slot-str-name:~s~%" slot-name slot-str-name)
      (when (getf data slot-kw)
        ;;(format t "found:~s~%" (getf data slot-kw))
        (setf (slot-value f slot-name) (getf data slot-kw))))))

