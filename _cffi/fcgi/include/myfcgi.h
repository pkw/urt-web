FCGX_Stream    *myfcgi_request_out(FCGX_Request *);
void 		myfcgi_text_html_header(FCGX_Request *);
int 		myfcgi_request_size();
void 		myfcgi_mainloop1(FCGX_Request *);
void 		myfcgi_serve1();
void 		myfcgi_serve2();
void 		myfcgi_serve_formtest();
void 		myfcgi_serve_param_route_test();
char           *myfcgi_get_param(FCGX_Request *, char *);
