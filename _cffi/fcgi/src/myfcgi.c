#include <assert.h>
#include <fcgiapp.h>
#include <string.h>
#include <stdlib.h>

#include "myfcgi.h"
#include "page1.h"

/*
While you are in a FCGX_Accept_r() loop frame you can use vars from
FCGX* calls (or request.* ?).  They will get cleaned up between
loop calls by FCGX_Finish_r().
*/

/*
 * not used delete me FCGX_Request   * get_fcgx_request_ptr() { FCGX_Request
 * *r; assert((r = malloc(sizeof(FCGX_Request))) != NULL); return r; }
 */

FCGX_Stream    *
myfcgi_request_out(FCGX_Request * req)
{
	return req->out;
}

FCGX_Stream    *
myfcgi_request_in(FCGX_Request * req)
{
	return req->in;
}

int
myfcgi_request_size()
{
	return sizeof(FCGX_Request);
}

void
myfcgi_text_html_header(FCGX_Request * r)
{
	FCGX_FPrintF(r->out, "Content-type: text/html\r\n\r\n");
}

int
parse_q(FCGX_Request * r)
{
	char          **ptr;
	char           *string;
	char           *found;
	char           *key;
	char           *param_str;

	ptr = r->envp;

	param_str = FCGX_GetParam("QUERY_STRING", ptr);
	string = strdup(param_str);

	if (string == NULL) {
		return -1;	/* error */
	}
	while ((found = strsep(&string, "&")) != NULL) {
		key = strsep(&found, "=");
		if (strcasecmp("q", key) == 0) {
			if (strcasecmp("1", found) == 0) {
				free(string);
				return 1;
			}
		}
	}

	free(string);
	return 0;
}


void
myfcgi_mainloop1(FCGX_Request * r)
{
	int 		q = 0;
	while (FCGX_Accept_r(r) == 0) {
		if (parse_q(r)) {
			q = 1;
		}
		page1_all(r);

		FCGX_Finish_r(r);
		if (q) {
			break;
		}
	}
}

void
myfcgi_serve1()
{
	FCGX_Request 	r;
	int 		sockfd;

	FCGX_Init();
	sockfd = FCGX_OpenSocket(":8000", 1024);
	FCGX_InitRequest(&r, sockfd, 0);

	myfcgi_mainloop1(&r);


	/* FCGX_Free(&r, sockfd); needed ? */
}

void
myfcgi_serve2()
{
	/* FCGX_Request   *r; */
	void           *r;
	int 		sockfd;

	assert((r = malloc(sizeof(FCGX_Request))) != NULL);

	FCGX_Init();
	sockfd = FCGX_OpenSocket(":8000", 1024);

	/*
	 * FCGX_InitRequest does not allocate data, just uses the pointer.
	 * Also this is to init any Request... FCGX_Init inits "the_request".
	 */
	FCGX_InitRequest(r, sockfd, 0);

	myfcgi_mainloop1(r);


	/* FCGX_Free frees the data in the struct, not the struct itself */
	FCGX_Free(r, sockfd);	/* needed ? */
	free(r);
}


void
myfcgi_serve_formtest()
{
	/* FCGX_Request   *r; */
	void           *r;
	int 		sockfd;

	assert((r = malloc(sizeof(FCGX_Request))) != NULL);

	FCGX_Init();		/* need to call this and InitRequest ? */
	sockfd = FCGX_OpenSocket(":8000", 1024);

	FCGX_InitRequest(r, sockfd, 0);

	while (FCGX_Accept_r(r) == 0) {
		page1_formtest(r);
		FCGX_Finish_r(r);
	}

	FCGX_Free(r, sockfd);	/* needed ? */
	free(r);
}


void
myfcgi_serve_param_route_test()
{
	/* FCGX_Request   *r; */
	void           *r;
	int 		sockfd;

	assert((r = malloc(sizeof(FCGX_Request))) != NULL);

	FCGX_Init();
	sockfd = FCGX_OpenSocket(":8000", 1024);

	FCGX_InitRequest(r, sockfd, 0);

	while (FCGX_Accept_r(r) == 0) {
		page1_param_route_test(r);
		FCGX_Finish_r(r);
	}

	FCGX_Free(r, sockfd);
	free(r);
}


char           *
myfcgi_get_param(FCGX_Request * r, char *name)
{
	return FCGX_GetParam(name, r->envp);
}
