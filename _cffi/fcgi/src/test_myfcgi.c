#include <assert.h>
#include <fcgiapp.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcgiapp.h>

#include "myfcgi.h"

void
menu()
{
	printf("[myfcgi]\n");
	printf("  1 - myfcgi serve1\n");
	printf("  2 - myfcgi serve2\n");
	printf("  3 - test pointers\n");
	printf("  4 - test size\n");
	printf("  5 - myfcgi serve formtest\n");
	printf("  6 - param route test\n");
}

void
test_p()
{
	FCGX_Request   *r;

	printf("test_p\n");
	printf("sizeof: FCGX_Request:%lu\n", sizeof(FCGX_Request));
	printf("sizeof:*FCGX_Request:%lu\n", sizeof(FCGX_Request *));
	/* r = malloc(sizeof(FCGX_Request)); */
	assert((r = malloc(sizeof(FCGX_Request))) != NULL);
	if (r == NULL) {
		printf("r is NULL\n");
	} else {
		printf("r (pointer):%p\n", r);
	}
}

int
main(int argc, char **argv)
{
	int 		choice = 0;

	if (argc > 1) {
		choice = atoi(argv[1]);
		switch (choice) {
		case 1:
			myfcgi_serve1();
			break;
		case 2:
			myfcgi_serve2();
			break;
		case 3:
			test_p();
			break;
		case 4:
			printf("size:%d\n", myfcgi_request_size());
			printf("size of char:%lu\n", sizeof(char));
			break;
		case 5:
			myfcgi_serve_formtest();
			break;
		case 6:
			myfcgi_serve_param_route_test();
			break;
		default:
			menu();
		}
	} else {
		menu();
	}

	return EXIT_SUCCESS;
}
