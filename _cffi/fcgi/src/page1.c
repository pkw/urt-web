#include <fcgiapp.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "myfcgi.h"
#include "page1.h"

void
print_q(FCGX_Request * r)
{
	FCGX_FPrintF(r->out, "<a href='/?q=1&msg=this-quits'>quit</a>\n");
}

void
page1_all(FCGX_Request * r)
{
	page1_header(r);
	page1_envs(r);
	print_q(r);
	page1_footer(r);
}

void
page1_form(FCGX_Request * r)
{
	FCGX_FPrintF(r->out, "<form method='post'>\n");	/* action */
	FCGX_FPrintF(r->out, "<input type='text' name='var1'/>\n");
	FCGX_FPrintF(r->out, "<input type='submit' value='Submits'>\n");
	FCGX_FPrintF(r->out, "</form>\n");
}

void
page1_formtest(FCGX_Request * r)
{
	char           *method;
	int 		cl;
	char           *buf;
	page1_header(r);
	FCGX_FPrintF(r->out, "<h1>form-test</h1>\n");

	method = FCGX_GetParam("REQUEST_METHOD", r->envp);
	if (strcasecmp(method, "GET") == 0) {
		page1_form(r);
	} else {
		/* assume POST */
		FCGX_FPrintF(r->out, "<h1>posted</h1>\n");
		cl = atoi(FCGX_GetParam("CONTENT_LENGTH", r->envp));
		FCGX_FPrintF(r->out, "<p>cl:%d</p>\n", cl);
		buf = malloc((cl + 1) * sizeof(char) + 1);
		FCGX_GetStr(buf, cl, r->in);
		buf[cl] = '\0';

		FCGX_FPrintF(r->out, "<p>buf:%s</p>\n", buf);
		free(buf);
	}

	page1_envs(r);
	print_q(r);
	page1_footer(r);
}

void
page1_param_route_test(FCGX_Request * r)
{
	page1_header(r);
	FCGX_FPrintF(r->out, "<h1>param route test</h1>\n");
	page1_envs(r);
	page1_footer(r);
}

void
page1_header(FCGX_Request * r)
{
	myfcgi_text_html_header(r);
	/*FCGX_FPrintF(r->out, "Content-type: text/html\r\n\r\n");*/
	FCGX_FPrintF(r->out, "<!DOCTYPE HTML>\n");
	FCGX_FPrintF(r->out, "<html>\n");
	FCGX_FPrintF(r->out, "<head>\n");
	FCGX_FPrintF(r->out, "<title>fcgi page1</title>\n");
	FCGX_FPrintF(r->out, "<link rel='stylesheet' href='/static/simple.css'>\n");
	FCGX_FPrintF(r->out, "</head>\n");
	FCGX_FPrintF(r->out, "<body>\n");
	FCGX_FPrintF(r->out, "<h1>fcgi page 1</h1>\n");
	FCGX_FPrintF(r->out, "<a href='/'>home</a>\n");
}

void
page1_envs(FCGX_Request * r)
{
	char          **ptr;
	ptr = r->envp;
	FCGX_FPrintF(r->out, "<ul>\n");
	while (*ptr != 0) {
		FCGX_FPrintF(r->out, "<li>%s</li>\n", *ptr);
		++ptr;
	}
	FCGX_FPrintF(r->out, "</ul>\n");
}

void
page1_footer(FCGX_Request * r)
{
	FCGX_FPrintF(r->out, "</body>\n");
	FCGX_FPrintF(r->out, "</html>\n");
}
