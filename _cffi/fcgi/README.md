This project is two things:

1. Standalone c code for running a fastcgi program
that presents a simple web site.

2. A shim library to use along with (lib)fcgi from 
common-lisp.

The common-lisp interface/shim is the main reason.
But if you see bits that present actual html content,
then that is just to test the standalone bits in order
to get the common-lisp helper stuff working.

---

This code was just merged into the common-lisp urt-web repo.

---

Do simple fastcgi page(s) with a c fastcgi lib.
Get it to work with OpenBSD fastcgi <-> httpd.


---

I ran into the same issue. Seems like a bug in FCGI. Workaround is
calling a library function directly for cleanup. OS_LibShutdown()
frees the memory init by FCGI_Accept() which internally calls
FCGX_Init(). For multithread apps, you have to call FCGX_Init()
yourself.

~~~
// Declare this (extern "C" is only required if from CPP)...
extern "C"
{
void OS_LibShutdown(void);
}

// From clean up code, call this...
OS_LibShutdown();

~~~

---

TODO IDEA:

Don't quit from C code, because maybe that will never
work will if cl is driving the c code.

Or research how to handle a crash in cffi.

OTHERWISE:Have cl initiate the quit.  This seems like the best 
short term idea.

---

spawn-fcgi comes from its own package
(which i think comes from lighttpd)

~~~
spawn-fcgi -p8000 -n fcgi1
~~~


git clone https://gitlab.com/jobol/mustach.git
cc -shared -fPIC myfcgi.o -o myfcgi.so
objdump -d myfcgi.o
