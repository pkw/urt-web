PROJ=	${.CURDIR:T}
CLROOT=	$(HOME)/common-lisp
WWWROOT=	/var/www/htdocs
#WWWROOT=	/var/www/vhost/urt.d34d.net/htdocs
SBCL="/usr/local/bin/sbcl"

.PHONY: link deploy-static clean

all: link

link:
	rm -rf $(CLROOT)/${PROJ}
	ln -s $(.CURDIR) $(CLROOT)/$(PROJ) 

deploy-static:
	mkdir -p $(WWWROOT)/static
	cp -r _static/* $(WWWROOT)/static/
	cp _static/favicon.ico $(WWWROOT)

clean:
	rm -f *.exe
	rm -f *.so



