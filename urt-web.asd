(defsystem "urt-web"
    :class :package-inferred-system
    :pathname "src"
    :build-operation program-op
    :build-pathname "../urt-web.exe"
    :entry-point "urt-web:main"
    :depends-on (:cl-dbi 
                 :sxql
                 :dbi ;; try cl-dbi instead ? (found from alive project)
                 :dbd-sqlite3 ;; this is needed for the .exe but ideally would want to allow postgres for example
                 :urt-web/all
                 :trivial-clock
                 :random-state
                 "cl-smtp"
                 "flexi-streams"
                 "file-config"))
;;(register-system-packages "urt-web/all" '(:urt-web))



