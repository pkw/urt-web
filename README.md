### short term ~2024-04-22 refactor some stuff:

- remove any djula references.
- use "src" and "t" in .asd setup
- use file-config to handle settings
- because of ^^^ generalize serve.lisp with things like port #
  so subclassing projects don't have to write their own serve.lisp
  use clue as a reference for latest and greatest.
- latest 'account' is in clue, it uses spinneret
  BUT some of the logic is from urt-web/account.
  MAYBE start by just chopping out the view bits (djula)
  
---

## URT-WEB

This is a specific implementation of a common-lisp web
framework.  The idea is that after/while implementing this,
the generic bits could be re-used, and maybe distilled into
a "web" common-lisp system.

(This is a case where I prefer to do the specific case
once or twice before abstracting into a general case.)

## Name

Alias this to something like "web".
There is prior art in some of the big more recent CL projects.
Ideally use a java style url name instead of urt-web like "net.d34d.web".
But just aliasing to "web" will be the good first step.

## Account

The /account/ handling stuff: login, registration, ... 
is handled between this (urt-web) and the implementing project (clue for exmaple).
Basically the backend stuff is baked in, and the implementing project 
has to setup the html/form pages that hook into it.
This is WIP, like maybe there is backend stuff bleeding into the implementing
project, but as dev happens if idead is to keep putting the 
UI agnostic stuff into urt-web, and do as little as possible
in the implementing project. 
(clue and dead-air both do this and they duplicate a lot of UI code.)

---

This repo has turned into using fastcgi libfcgi cffi commonlisp
to do web pages on OpenBSD using builtin httpd. 

~~~
;; disable LDB in sbcl
(sb-alien:alien-funcall
    (sb-alien:extern-alien "disable_lossage_handler" (function sb-alien:void)))

~~~

~~~
# httpd.conf setup:
server "default" {
        listen on * port 80
        location * { fastcgi socket tcp 127.0.0.1 9020 }
}
~~~
